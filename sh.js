class SyntaxHighlight {
    static highlight(html) {
        let ret = html;
        ret = ret.replace(/\d/g, "<sh-num>$&</sh-num>");
        ret = ret.replace(/`[\s\S]*`/g, "<sh-temp-str>$&</sh-temp-str>");
        ret = ret.replace(/"[\s\S]*"|'[\s\S]*'/g, "<sh-str>$&</sh-str>");
        ret = ret.replace(/true|false/g, "<sh-bool>$&</sh-bool>");
        ret = ret.replace(/[\s\S]*\n/g, "<sh-line>$&</sh-line>");
        return ret;
    }
}